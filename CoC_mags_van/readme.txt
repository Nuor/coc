mag_sys 2.1.2	for Call of Chernobyl
adapted from thunderfreaks 2.1
to install copy or merge gamedata

This mod changes ammo system to force player to load magazines before they can be used by weapons.
Upon slotting or unhostering a weapon for the first time an icon (currently a vest/armor) will appear in your inventory. Use this item and a loading screen will appear add or remove specific ammo types for each slotted weapon.
Only slotted weapon magazines may be loaded but once magazines exist they will remain if you unslot the weapon.

Currently vest bonus slots aren't in the game but you can spawn the vest bonus addons and use them which will expand base vest pool size.

changes for 2.1.2
1.fixed bug where ammo was disappearing on load.
2. Changed magazine storage so instead of having separate magazine slots for each weapon slot there is a common pool of usable vest "pockets" that can be fill from either weapon slot type. Base slot pool is 8 pistols take 1 pooled slot; rifles take 2 pooled slots. Change "vest_base" variable to alter this pooled quantity.